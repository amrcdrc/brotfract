﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;


namespace assignment
{
    public partial class Form1 : Form
    {
        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real x-axis
        private const double SY = -1.125; // start value imaginary y-axis
        private const double EX = 0.6;    // end value real x-axis
        private const double EY = 1.125;  // end value imaginary y-axis
        private static int x1, y1, xs, ys, xe, ye; // xs ,ys = mousedown x,y coordinates || xe,ye = mousemove x,y coordinates
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;
        private bool mouseDown = false; // define mousedown i.e when mouse clicks
        private Bitmap picture; // define bitmap 
        private Graphics g; // create graphics
        private Graphics g1; // create graphics
        private Cursor c1, c2; // set cursor for picturebox zoom and load
        private HSB HSBcol; // color class from java source code 
        private Pen pen; // define pen 
       
        

        private void startToolStripMenuItem_Click(object sender, EventArgs e) // method for start button
        {
            start(); // calls start 
        }

        private void cloneToolStripMenuItem_Click(object sender, EventArgs e) // method for clone button
        {
            Form1 obj = new Form1(); // create new form 1  object
            obj.pictureBox1.Image = picture; // clones current image


            obj.Show(); // show form1

            


        }

        

        private void toolStripMenuItem1_Click(object sender, EventArgs e) // method for save button
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Images|*.png;*.bmp;*.jpg";
            ImageFormat format = ImageFormat.Png; // default
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string ext = System.IO.Path.GetExtension(sfd.FileName);
                switch (ext)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        format = ImageFormat.Bmp;
                        break;
                }
                pictureBox1.Image.Save(sfd.FileName, format);
            }
        }


       
 

       
        private void reloadToolStripMenuItem_Click(object sender, EventArgs e) //method for reload button
        {
            Stop(); // calls stop() fucntion
            start(); // calls start() function
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try { Clipboard.SetImage(pictureBox1.Image); }
            catch { }
            pictureBox2.Image = Clipboard.GetImage();
            pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit(); // exits app
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart(); // restarts app
        }

        int tmr = 255; // for background color change
        int imr = 255;
        int cmr = 255;
        
        private void timer1_Tick(object sender, EventArgs e) // timer event
        {

            tmr-=10;
            imr-=20;
            cmr-=30; if (tmr < 80 || imr < 80 || cmr < 80)
            {
                tmr = 230;
                cmr = 235;
                imr = 235;
            }
            this.BackColor = Color.FromArgb(tmr, imr, cmr); // changes background color

           


        }

        private void button2_Click(object sender, EventArgs e)
        {

            timer1.Start(); // starts the timer

        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer1.Stop(); // stops the timer
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This is a demonstration of a MandelBrot Set");
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e) // medtod for stop button
        {
            Stop();  // calls stop() fucntion
        }
        public void Stop() // define stop() 
        {
            pictureBox1.Image = null; // set picturebox.1's image to null i.e no image displayed
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() != DialogResult.Cancel)
            {
                this.BackColor = colorDialog1.Color;
            }
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)

        {
            
            printPreviewDialog1.ShowDialog();
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e) //prints the document.
        {
            if (picture == null) //check if the application is stopped.
            {
                MessageBox.Show("The application is stop so, image cannot be printed.");
            }
            else
            {

                e.Graphics.DrawImage(picture, 0, 0);
            }
        }


        // CODE FOR ZOOM STARTS HERE


        private void pictureBox1_MouseDown(object sender, MouseEventArgs e) // method when mouse is clicked 
        {
            //e.consume();
            if (action)
            {
                mouseDown = true;
                xs = e.X; // assign X coordinate mouseclick value to xs
                ys = e.Y; // assign Y coordinate mouseclick value to ys

                label1.Text = "Start :   X = " + e.X + "  Y = " + e.Y;
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e) // when mouse moves
        {
            //e.consume();

            if (action && mouseDown) // if mouse moves alogn with mouse button clicked
            {
                xe = e.X; // assign mousemove X coordinate to xe
                ye = e.Y; // assign mousemove Y coordinate to ye
                rectangle = true; // draw rectangle 
                pictureBox1.Refresh(); // to see the drawn rectangle 
            }
        }

       

       



        private void pictureBox1_MouseUp(object sender, MouseEventArgs e) // when mouse click released
        {
            int z, w;
            label2.Text = "End :  X = " + e.X + "  Y = " + e.Y;

            //e.consume();
            if (action)
            {
                xe = e.X; // get final mouse release X coordinate
                ye = e.Y; // get final mouse release Y coordinate
                if (xs > xe) // checks if mouse is dragged to right
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye) // checks if mouse is dragged to bottom
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);// difference between final and initial X coordinates
                z = (ye - ys);// difference between final and inital Y coordinates
                if ((w < 0) && (z < 0)) initvalues();// zoom if the rectangle is very small i.e click zoom where recltangle's w =0, z=0 || zoom to single pixel;
                else // formula for zoom
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot(); // calls mandelbrot image after zooming in 
                rectangle = false; // removes rectangle after mouseup 
                mouseDown = false;// removes rectangle after zoom in .. rectangle stays because of mouse down
            }
        }

        // END CODE FOR ZOOM


        public Form1()
        {
            InitializeComponent();
            HSBcol = new HSB();
            this.pictureBox1.Size = new System.Drawing.Size(pictureBox1.Width, pictureBox1.Height); // set picturebox1 size to its width and height in design 
            finished = false;
            c1 = Cursors.WaitCursor; // creates circular wait cursor
            c2 = Cursors.Cross; // creates crosshair cursor
            x1 = pictureBox1.Width; //x1 = getsize().width;
            y1 = pictureBox1.Height; //y1 = getsize().height;
            xy = (float)x1 / (float)y1; // aspect ratio 
            
            picture = new Bitmap(pictureBox1.Width, pictureBox1.Height); //picture = createImage(x1, y1);
            //g1 = picture.getGraphics();
            g1 = Graphics.FromImage(picture);
            finished = true;

            start(); // calls start method i.e mandelbrot image

        }

        public void start() // define start() same from java
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot(); // calls mandelbrot
            


        }

        public void destroy() // delete all instances  from java source code
        {
            if (finished)
            {
                this.pictureBox1.MouseDown -= this.pictureBox1_MouseDown; // mousedown listner in picturebox
                this.pictureBox1.MouseMove -= this.pictureBox1_MouseMove; // mousemove listner in picturebox
                this.pictureBox1.MouseUp -= this.pictureBox1_MouseUp; // mouseup listner in picturebox

                picture = null;
                g1 = null;
                c1 = null;
                c2 = null;
                System.GC.Collect(); // garbage collection
            }
        }

        private void initvalues() // reset start values
        {
            xstart = SX; // resets xstart i.e real value 
            ystart = SY; // resets ystart i.e imaginary value
            xende = EX; // resets xstart i.e real value 
            yende = EY; // resets ystart i.e imaginary value
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;


            action = false;
            
            pictureBox1.Cursor = c2;// sets  crosshair cursor inside picturebox
            label3.Text = "Everything Working fine";


            for (x = 0; x < x1; x += 2)
            {
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value

                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes

                        HSBcol.fromHSB(h, 0.8f, b); //convert hsb to rgb then make a Java Color
                        Color col = Color.FromArgb(Convert.ToByte(HSBcol.rChan), Convert.ToByte(HSBcol.gChan), Convert.ToByte(HSBcol.bChan)); // convert to byte because color value only to 256

                        pen = new Pen(col); // user color in pen from above

                        alt = h;
                    }
                    g1.DrawLine(pen, new Point(x, y), new Point(x + 1, y)); // drawing g1.drawLine(x, y, x + 1, y) in c# || use above pen color
                    
                }
                
                Cursor.Current = c1; //  load cursor because inside the loop
                

                action = true; // from java source 
                pictureBox1.Image = picture; // set picturebox image to bit map img

            }
            
            
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i; 
                i = 2.0 * r * i + ywert; 
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e) // private void pain (graphics g) in java source code  
        {
            update(); // call update
        }

        public void update() // define update 
        {
            Image tempPic = picture; // set temppic 
            Graphics g = Graphics.FromImage(tempPic); // define graphics g  from temppic

            if (rectangle)
            {
                SolidBrush blueBrush = new SolidBrush(Color.Blue); // define brush color 

                Rectangle rect;

                if (xs < xe)
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xs, ys, (xe - xs), (ye - ys));
                    }
                    else
                    {
                        rect = new Rectangle(xs, ye, (xe - xs), (ys - ye));
                    }
                }
                else
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xe, ys, (xs - xe), (ye - ys));
                    }
                    else
                    {
                        rect = new Rectangle(xe, ye, (xs - xe), (ys - ye));
                    }
                }

                g.FillRectangle(blueBrush, rect); // create rect
                GC.Collect(); // garbage collect

            }
        }
        

    }

    //COLOR 
    class HSB
    {
        public float rChan, gChan, bChan;
        
        public HSB()
        {
            rChan = gChan = bChan = 0;
        }
        public void fromHSB(float h, float s, float b)
        {
            if (s == 0)
            {
                rChan = gChan = bChan = (int)(b * 255.0f + 0.5f);
            }
            else
            {
                h = (h - (float)Math.Floor(h)) * 6.0f;
                float f = h - (float)Math.Floor(h);
                float p = b * (1.0f - s);
                float q = b * (1.0f - s * f);
                float t = b * (1.0f - (s * (1.0f - f)));
                switch ((int)h)
                {
                    case 0:
                        rChan = (int)(b * 255.0f + 0.5f);
                        gChan = (int)(t * 255.0f + 0.5f);
                        bChan = (int)(p * 255.0f + 0.5f);
                        break;
                    case 1:
                        rChan = (int)(q * 255.0f + 0.5f);
                        gChan = (int)(b * 255.0f + 0.5f);
                        bChan = (int)(p * 255.0f + 0.5f);
                        break;
                    case 2:
                        rChan = (int)(p * 255.0f + 0.5f);
                        gChan = (int)(b * 255.0f + 0.5f);
                        bChan = (int)(t * 255.0f + 0.5f);
                        break;
                    case 3:
                        rChan = (int)(p * 255.0f + 0.5f);
                        gChan = (int)(q * 255.0f + 0.5f);
                        bChan = (int)(b * 255.0f + 0.5f);
                        break;
                    case 4:
                        rChan = (int)(t * 255.0f + 0.5f);
                        gChan = (int)(p * 255.0f + 0.5f);
                        bChan = (int)(b * 255.0f + 0.5f);
                        break;
                    case 5:
                        rChan = (int)(b * 255.0f + 0.5f);
                        gChan = (int)(p * 255.0f + 0.5f);
                        bChan = (int)(q * 255.0f + 0.5f);
                        break;
                }

            }

        }

    }




}
